using System;
using System.IO;
using System.Collections;

namespace Oni.Utils
{
	static class File
	{
		public static uint8[] LoadBytesFromFile(String fileName)
		{
			var fileData = scope List<uint8>();
			System.IO.File.ReadAll(fileName, fileData);
			var fileDataArr = new uint8[fileData.Count];
			Internal.MemCpy(fileDataArr.Ptr, fileData.Ptr, fileDataArr.Count);
			return fileDataArr;
		}

		public static String LoadFile(String fileName)
		{
			var fileData = scope List<uint8>();
			System.IO.File.ReadAll(fileName, fileData);
			var fileDataString = new String((char8*) fileData.Ptr, fileData.Count);
			return fileDataString;
		}
	}
}