using System;

#if DEBUG
using System.Diagnostics;
#endif
namespace Oni.Utils
{
	static
	{
		public static void Panic(String message)
		{
#if DEBUG
			Debug.Assert(false, message);
			
#else
			System.Runtime.Assert(false, message);	
#endif
		}

		public static void Panic()
		{
#if DEBUG
			Debug.Assert(false);
#else
			System.Runtime.Assert(false);	
#endif
		}

		public static void Assert(bool condition, String message)
		{
#if DEBUG
			Debug.Assert(condition, message);
#else
			System.Runtime.Assert(condition, message);	
#endif
		}

		public static void Assert(bool condition)
		{
#if DEBUG
			Debug.Assert(condition);
#else
			System.Runtime.Assert(condition);	
#endif
		}
	}
}