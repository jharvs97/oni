using System;

namespace Oni.Utils
{
	enum LogLevel
	{
		None 	= 0,
		Info 	= 1,
		Trace	= 2,
	}

	static class Logger
	{
		private static LogLevel mLogLevel;

		public static LogLevel LogLevel
		{
			get 
			{
				return mLogLevel;
			}

			set
			{
#if DEBUG
				mLogLevel = value;
#endif
			}
		}

		public static this()
		{
#if DEBUG
			mLogLevel = .Info;
#else
			mLogLevel = .None;
#endif
		}

		public static void Log(LogLevel logLevel, StringView message)
		{
			if (mLogLevel != .None && logLevel <= mLogLevel)
			{
				Console.WriteLine($"[{logLevel}] {message}");
			}
		}

		public static void Log(LogLevel logLevel, Object object)
		{
			if (mLogLevel != .None && logLevel <= mLogLevel)
			{
				String builder = scope String(1024);
				if (object == null)
				{
					builder.Append("null");
				}
				else
				{
					object.ToString(builder);
				}

				Console.WriteLine($"[{logLevel}] {builder}");
			}
		}
	}
}