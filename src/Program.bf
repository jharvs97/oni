using System;
using System.IO;
using System.Collections;

using Oni.Bytecode;
using Oni.Utils;

namespace Oni
{
	class Program
	{
		private static void PrintUsage()
		{
			Console.WriteLine("usage: vm <file>");
		}

		public static void Main(String[] args)
		{
			Logger.LogLevel = .Trace;
			Logger.Log(.Info, "Oni is starting");

			if (args.Count != 1)
			{
				PrintUsage();
				return;
			}

			var programSource = Oni.Utils.File.LoadFile(args[0]);
			defer delete programSource;

			Assembler assembler = scope Assembler();
			Interpreter interp = scope Interpreter();

			uint8[] byteCode = assembler.FromString(programSource);
			System.IO.File.WriteAll("data/test.bni", byteCode);

			defer delete byteCode;

			interp.Run(byteCode);

#if DEBUG
			Logger.Log(.Info, interp);
			Console.WriteLine("Press 'Enter' to exit debugger");
			Console.Read();
#endif
		}
	}
}