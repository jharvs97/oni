using System;
using System.Collections;

using Oni.Utils;

namespace Oni.Bytecode
{
	class Interpreter
	{
		private typealias OpFunc = function uint8*(Interpreter this);
		private const int OP_FUNC_BUFFER_SIZE = (int) typeof(OpCode).MaxValue + 1;
		private OpFunc[OP_FUNC_BUFFER_SIZE] mOpFuncs = .(
			=> NoOp,
			=> NoOp,
			=> PushCharOp,
			=> EmitOp,
			=> PushIntOp,
			=> AddIntOp,
			=> CompareOp,
			=> JumpOp,
			=> LoadIntOp,
			=> StoreIntOp,
			=> JumpIfFalseOp
		);

		private uint8[] mByteCode;
		private uint8* mInstructionPtr;
		private ValueStack mDataStack = new ValueStack(4096) ~ delete _;

		public uint8* InstructionPtr
		{
			get => mInstructionPtr;
			set
			{
				Assert(value < (mByteCode.Ptr + mByteCode.Count));
				mInstructionPtr = value;
			}
		}

		public ValueStack DataStack
		{
			get => mDataStack;
		}

		public override void ToString(String builder)
		{
			int stackIndex = 0;
			for (let value in mDataStack)
			{
				builder.Append(scope $"[{stackIndex}] {value}");
			}
		}

		public void Run(uint8[] byteCode)
		{
			mByteCode = byteCode;
			mInstructionPtr = mByteCode.Ptr;

			ExecuteLoop: while (true)
			{
				uint8 rawOp = *mInstructionPtr;
				OpCode op = (OpCode) rawOp;

				let offset = mInstructionPtr - mByteCode.Ptr;
				Logger.Log(.Trace, scope $"Current OpCode is {op} at byte offset {offset}");

				switch (op)
				{
				case .Halt:
					break ExecuteLoop;
				default:
					InstructionPtr = mOpFuncs[rawOp](this);
					break;
				}

				Assert(mInstructionPtr < mByteCode.Ptr + mByteCode.Count);
			}
		}

		private int32 ReadInt32(uint8* startAddress)
		{
			int32 result = 0;
			for (int i < 4)
			{
				uint8 byte = *(startAddress + i);
				result += (((int32) byte)) << (i*8);
			}
			return result;
		}

		private uint8* NoOp()
		{
			return InstructionPtr + 1;
		}

		private uint8* PushCharOp()
		{
			char8 char = (char8) *(mInstructionPtr + 1);

			Logger.Log(.Trace, scope $"{OpCode.PushChar}: Pushing '{char}' onto the stack");

			mDataStack.Push(.Char(char));
			return mInstructionPtr + 2;
		}

		private uint8* PushIntOp()
		{
			int32 result = ReadInt32(mInstructionPtr + 1);
			Logger.Log(.Trace, scope $"{OpCode.PushInt}: Pushing {result} onto the stack");
			mDataStack.Push(.Int(result));

			return mInstructionPtr + 5;
		}

		private uint8* EmitOp()
		{
			Value val = mDataStack.Pop();
			switch (val)
			{
			case .Char(let u8):
				char8 c = (char8) u8;
				System.Console.Write(c);
				break;
			default:
				Panic("Non-character found on stack while emitting");
			}

			return mInstructionPtr + 1;
		}

		private uint8* AddIntOp()
		{
			Value rhs = mDataStack.Pop();
			Value lhs = mDataStack.Pop();

			Logger.Log(.Trace, scope $"{OpCode.AddInt}: Performing {lhs} + {rhs}");

			Value result = lhs + rhs;

			switch (result)
			{
			case .Int:
				Logger.Log(.Trace, scope $"{OpCode.AddInt}: Pushing {result} onto the stack");
				mDataStack.Push(result);
			default:
				Panic("Invalid add operation");
			}

			return mInstructionPtr + 1;
		}

		private uint8* JumpOp()
		{
			int32 jumpTo = ReadInt32(mInstructionPtr + 1);
			Logger.Log(.Trace, scope $"{OpCode.Jump}: Jumping to byte offset {jumpTo}");
			return &mByteCode[jumpTo];
		}

		private uint8* CompareOp()
		{
			Value rhs = mDataStack.Pop();
			Value lhs = mDataStack.Pop();

			Logger.Log(.Trace, scope $"{OpCode.Compare}: Performing {lhs} == {rhs}");

			Value compareValue = lhs - rhs;

			switch (compareValue)
			{
			case .Int(let int):
				Logger.Log(.Trace, scope $"{OpCode.Compare}: Pushing {int} onto the stack");
				mDataStack.Push(.Int(int));
			default:
				Panic("Can't compare these two values");
			}

			return mInstructionPtr + 1;
		}

		private uint8* LoadIntOp()
		{
			int32 loadFromAddress = ReadInt32(mInstructionPtr + 1);
			Value loadedValue = mDataStack[loadFromAddress];

			switch (loadedValue)
			{
			case .Int(int32 loadedInt):
				Logger.Log(.Trace, scope $"{OpCode.LoadInt}: Loading {loadedInt} from {loadFromAddress}");
				mDataStack.Push(.Int(loadedInt));
				break;
			default:
				Panic("Tried to load non-int");
			}

			return mInstructionPtr + 5;
		}

		private uint8* StoreIntOp()
		{
			int32 storeToAddress = ReadInt32(mInstructionPtr + 1);

			Value valueToStore = mDataStack.Pop();

			switch (valueToStore)
			{
			case .Int(let int):
				Logger.Log(.Trace, scope $"{OpCode.StoreInt}: Storing {int} at {storeToAddress}");
				mDataStack[storeToAddress] = .Int(int);
				break;
			default:
				Panic("Non-int on top of the stacc");
			}

			return mInstructionPtr + 1;
		}

		private uint8* JumpIfFalseOp()
		{
			Value conditionValue = mDataStack.Pop();

			switch (conditionValue)
			{
			case .Int(let condition):
				if (condition == 0)
				{
					return JumpOp();
				}
				else
				{
					Logger.Log(.Trace, scope $"{OpCode.JumpIfFalse}: Not jumping");
					return mInstructionPtr + 5; // Skip the jump address
				}
			default:
				Panic("Non-valid condition value on top of the stack");
			}

			return NoOp();
		}
	}
}