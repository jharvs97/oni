using Oni.Utils;

namespace Oni.Bytecode
{
	enum Value
	{
		case None(void);
		case Int(int32 int);
		case Char(char8 char);
		case Ptr(void* ptr);

		public int32 GetInt()
		{
			switch (this)
			{
			case .Int(let int):
				 return int;
			default:
				Panic("Not an int!");
				return 0;
			}
		}

		public static Value operator+(Value a, Value b)
		{
			switch (a)
			{
			case .Int(let aInt):
				return aInt + b;
			default:
				return .None;
			}
		}

		public static Value operator+(int32 aInt, Value b)
		{
			switch (b)
			{
			case .Int(let bInt):
				return .Int(aInt + bInt);
			default:
				return .None;
			}
		}

		public static Value operator-(Value a, Value b)
		{
			switch (a)
			{
			case .Int(let aInt):
				return aInt - b;
			default:
				return .None;
			}
		}

		public static Value operator-(int32 aInt, Value b)
		{
			switch (b)
			{
			case .Int(let bInt):
				return .Int(aInt - bInt);
			default:
				return .None;
			}
		}
	}
}