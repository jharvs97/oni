using System;
using System.Collections;
using System.Diagnostics;
using Oni.Utils;

namespace Oni.Bytecode
{
	class ValueStack : IEnumerable<Value>
	{
		public int mCapacity;
		private int mTop = 0;
		private Value[] mBuffer ~ delete _;

		public int Count => mTop;

		public Value this[int index]
		{
			get
			{
				Assert(index >= 0 && index < mTop);
				return mBuffer[index];
			}

			set
			{
				Assert(index >= 0 && index < mTop);
				mBuffer[index] = value;
			}
		}

		public this(int capacity)
		{
			mCapacity = capacity;
			mBuffer = new Value[mCapacity];
		}

		public int Push(Value val)
		{
			Assert(mTop < mCapacity);
			mBuffer[mTop++] = val;
			return mTop;
		}

		public Value Pop()
		{
			Assert(mTop > 0);
			return mBuffer[--mTop];
		}

		public Value Peek()
		{
			Assert(mTop > 0);
			return mBuffer[mTop - 1];
		}

		public Enumerator GetEnumerator()
		{
			return Enumerator(this);
		}

		public struct Enumerator : IEnumerator<Value>
		{
			private ValueStack mValueStack;
			private int mIndex;
			private Value* mCurrent;

			public Value Current
			{
				get => *mCurrent;
				set => *mCurrent = value;
			}

			public this(ValueStack valueStack)
			{
				mValueStack = valueStack;
				mIndex = 0;
				mCurrent = null;
			}

			public Result<Value> GetNext() mut
			{
				if (!MoveNext())
					return .Err;
				return Current;
			}

			public bool MoveNext() mut
			{
				if (mIndex < mValueStack.mTop)
				{
					mCurrent = &mValueStack.mBuffer[mIndex];
					mIndex ++;
					return true;
				}
				return false;
			}
		}
	}
}