using System;
using System.Collections;
using Oni.Utils;

namespace Oni.Bytecode
{
	class Generator
	{
		private List<uint8> mByteCode = new List<uint8>() ~ delete _;

		public int32 CurrentByteOffset => (int32) mByteCode.Count;

		public uint8[] GetByteCode()
		{
			uint8[] result = new uint8[mByteCode.Count];
			Internal.MemCpy(result.Ptr, mByteCode.Ptr, result.Count);
			return result;
		}

		public void Reserve(int n)
		{
			for (let i < n)
			{
				mByteCode.Add(0xBA);
			}
		}

		public void Emit(OpCode op)
		{
			mByteCode.Add((uint8) op);
		}

		public void Emit(Value value)
		{
			switch (value)
			{
			case .Int(let int):
				EmitInt(int);
				break;
			default:
				Panic("Failed to emit bytecode, unexpected value");
			}
		}

		public void EmitAt(Value value, int offset)
		{
			switch (value)
			{
			case .Int(let int):
				EmitInt(int, offset);
				break;
			default:
				Panic("Failed to emit bytecode, unexpected value");
			}
		}

		private void EmitChar(char8 char)
		{
			mByteCode.Add((uint8) char);
		}

		private void EmitInt(int32 int)
		{
			for (int i < 4)
			{
				uint8 byte = (uint8) ((int >> (i*8)) & 0xFF);
				mByteCode.Add(byte);
			}
		}

		private void EmitInt(int32 int, int offset)
		{
			for (int i < 4)
			{
				uint8 byte = (uint8) ((int >> (i*8)) & 0xFF);
				mByteCode[offset+i] = byte;
			}
		}

		public override void ToString(String strBuffer)
		{
			String byteCodeString = scope String((char8*)mByteCode.Ptr, mByteCode.Count);
			strBuffer.Append(byteCodeString);
		}
	}
}