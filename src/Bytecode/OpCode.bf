namespace Oni.Bytecode
{
	enum OpCode : uint8
	{
		None 		= 0x0,
		Halt 		= 0x1,
		PushChar 	= 0x2,
		Emit 		= 0x3,
		PushInt 	= 0x4,
		AddInt 		= 0x5,
		Compare 	= 0x6,
		Jump		= 0x7,
		LoadInt		= 0x8,
		StoreInt	= 0x9,
		JumpIfFalse = 0xA,
	}
}