using System;
using System.Collections;
using Oni.Utils;
using Oni.Bytecode;

namespace Oni
{
	class Assembler
	{
		struct Instruction
		{
			public int32 LineNumber;
			public int32 StartingByte;
			public OpCode OpCode;
			public Value? Value;

			public this(int32 lineNumber, int32 startingByte)
			{
				LineNumber = lineNumber;
				StartingByte = startingByte;
				OpCode = .None;
				Value = null;
			}
		}

		private int mBytesWritten;
		private StringView mProgramSource;
		private Generator mGenerator = new Generator() ~ delete _;
		private List<int> mAddrsToResolve = new List<int>() ~ delete _;
		private List<Instruction> mInstructions = new List<Instruction>() ~ delete _;

		public uint8[] FromString(StringView programSource)
		{
			mProgramSource = programSource;

			mProgramSource.Trim();
			let lines = mProgramSource.Split('\n');
			int32 lineNum = 1;

			for (var line in lines)
			{
				int32 startingByte = mGenerator.CurrentByteOffset;
				Instruction instruction = .(lineNum, startingByte);

				line.Trim();
				Assert(line.Length > 0);

				var parts = line.Split(' ');

				let strOp = parts.GetNext();
				Assert(strOp != .Err);

				OpCode opCode = ParseOpCodeOrPanic(strOp);
				instruction.OpCode = opCode;

				mGenerator.Emit(opCode);

				let maybeStrValue = parts.GetNext();

				if (maybeStrValue != .Err)
				{
					Value value = ParseValueOrPanic(maybeStrValue);

					instruction.Value = value;

					if (opCode == .Jump || opCode == .JumpIfFalse)
					{
						switch (value)
						{
						case .Int(let jumpLine):
							if (jumpLine > lineNum)
							{
								mGenerator.Reserve(4);
								mAddrsToResolve.Add(lineNum);
							}
							else
							{
								Instruction instructionAtJumpLoc = mInstructions[jumpLine - 1];
								mGenerator.Emit(.Int(instructionAtJumpLoc.StartingByte));
							}
						default:
							Panic("Tried to jump with non-int");
						}	
					}
					else
					{
						mGenerator.Emit(value);
					}
				}

				mInstructions.Add(instruction);
				mBytesWritten += (mGenerator.CurrentByteOffset - startingByte);
				lineNum += 1;
			}

			ResolveAddresses();

			return mGenerator.GetByteCode();
		}

		private void ResolveAddresses()
		{
			for (let lineNums in mAddrsToResolve)
			{
				Instruction instruction = mInstructions[lineNums-1];
				Assert(instruction.Value.HasValue);

				switch (instruction.Value.Value)
				{
				case .Int(let int):
					let jumpToInstruction = mInstructions[int - 1];
					let jumpToByte = (int32) jumpToInstruction.StartingByte;
					mGenerator.EmitAt(.Int(jumpToByte), instruction.StartingByte + 1);
					break;
				default:
					Panic("Non int address found");
				}

			}
		}

		private OpCode ParseOpCodeOrPanic(StringView strOp)
		{
			switch (strOp)
			{
			case "push":
				return .PushInt;
			case "load":
				return .LoadInt;
			case "cmp":
				return .Compare;
			case "jne":
				return .JumpIfFalse;
			case "add":
				return .AddInt;
			case "jump":
				return .Jump;
			case "halt":
				return .Halt;
			default:
				Panic(scope $"Could not parse op code {strOp}");
				return .None;
			}
		}

		private Value ParseValueOrPanic(StringView strValue)
		{
			let maybeInt = int.Parse(strValue);

			switch (maybeInt)
			{
			case .Ok(let val):
				return .Int((int32) val);
			default:
				Panic(scope $"Could not parse {maybeInt} into an int32");
			}

			return .None;
		}
	}
}